"""
Contracts that apply to salt states
"""


async def post(hub, ctx):
    ret = await hub.pop.loop.unwrap(ctx.ret)
    # Salt states don't use "new_state", so assume the input kwargs are representative of the new_state
    ret["new_state"] = hub.lib.copy.deepcopy(ctx.kwargs)
    ret["new_state"].pop("ctx", None)
    # Salt states don't use "resource_Id", but their usage of "name" is similar enough in function
    ret["resource_id"] = ret["name"]
    return ret
