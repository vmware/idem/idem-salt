"""
Contracts that apply to salt exec modules
"""


def call(hub, ctx):
    result = dict(result=True, comment=[], ret=None)

    try:
        result["ret"] = ctx.func(*ctx.args, **ctx.kwargs)
    except Exception as e:
        result["result"] = False
        result["comment"].append(
            f"Failed to run {ctx.ref}: {e.__class__.__name__}: {e}"
        )

    return result


def post(hub, ctx):
    # If the one call contract was overridden then ensure the output is still in idem's format
    if not isinstance(ctx.ret, dict):
        return dict(result=True, comment=[], ret=ctx.ret)
    elif not set(ctx.ret.keys()).issuperset({"ret", "result", "comment"}):
        return dict(result=True, comment=[], ret=ctx.ret)
    return ctx.ret
