__func_alias__ = {"exec_": "exec"}


def exec_(hub, path: str, context):
    """
    Resolve salt modules paths for the hub.exec.salt dynamic sub
    """
    if path.startswith("salt."):
        salt_path = path.split("salt.", maxsplit=1)[1]
    else:
        salt_path = path

    function = hub.salt.functions[salt_path]

    ref, name = path.rsplit(".", maxsplit=1)

    return hub.lib.pop.contract.create_contracted(
        hub,
        contracts=[hub.tool.salt.exec_contracts],
        func=function,
        ref=ref,
        name=name,
        implicit_hub=getattr(function, "implicit_hub", False),
    )


def state(hub, path: str, context):
    """
    Resolve salt state paths for the hub.states.salt dynamic sub
    """
    if path.startswith("salt."):
        # We are explicitly looking at a salt state
        salt_path = path.split("salt.", maxsplit=1)[1]
    else:
        salt_path = path

    ref, name = path.rsplit(".", maxsplit=1)

    if ref.endswith(".signature.parameters"):
        # Patch for ReverseSub when idem checks if "acct_profile" is in the function signature
        salt_path = salt_path.split(".signature.parameters", maxsplit=1)[0]
        if salt_path.endswith("is_pending"):
            return hub.tool.salt.resolve.is_pending
        signature = hub.lib.inspect.signature(hub.salt.states[salt_path])

        def _sig(*a, **kw):
            func = getattr(signature.parameters, name)
            return func(*a, *kw)

        return _sig

    # Patch for ReverseSub Idem checks for param_aliases
    if ref.endswith(".param_aliases"):
        # Salt doesn't use param_aliases, so skip it
        d = {}

        def _empty_dict(*a, **kw):
            func = getattr(d, name)
            return func(*a, *kw)

        return _empty_dict

    # Patch for ReverseSub Idem checks for an is_pending function
    if name == "is_pending":
        return hub.tool.salt.resolve.is_pending

    # Patch for ReverseSub Idem checks for a mod_aggregate function
    if name == "mod_aggregate":
        return hub.tool.salt.resolve.mod_aggregate

    # Inject the saltenv into the state
    if "__env__" not in hub.salt.states.inject_globals:
        hub.salt.states.inject_globals["__env__"] = hub.salt.opts["saltenv"]

    async def _async_state_caller(*args, **kwargs):
        # TODO it might be better to do this through hub.salt.states.call()
        func = hub.salt.states[salt_path]

        # Run the state function in an async executor
        return await hub.pop.loop.wrap(func, *args, **kwargs)

    return hub.lib.pop.contract.create_contracted(
        hub,
        contracts=[hub.tool.salt.state_contracts],
        func=_async_state_caller,
        ref=ref,
        name=name,
        implicit_hub=False,
    )


def render(hub, path: str, context):
    """
    Resolve salt renderer paths for the hub.rend sub
    """
    if path.startswith("salt."):
        salt_path = path.split("salt.", maxsplit=1)[1]
    else:
        salt_path = path

    mod = salt_path.split(".render", maxsplit=1)[0]

    function = hub.salt.rend[mod]

    ref, name = path.rsplit(".", maxsplit=1)

    def _rend_wrapper(data, params=None):
        with hub.lib.io.BytesIO(data) as io:
            ret = function(io, params=params)
        return ret

    return hub.lib.pop.contract.create_contracted(
        hub,
        contracts=[],
        func=_rend_wrapper,
        ref=ref,
        name=name,
        implicit_hub=getattr(function, "implicit_hub", False),
    )


def is_pending(hub, ret: dict, state: dict, **kwargs) -> bool:
    """
    Idem will always succeed in searching for an is_pending function because hub.salt.states is a ReverseSub.
    This function bypasses is_pending by simply returning False when it is called.
    """
    return False


def mod_aggregate(hub, name: str, chunk: dict) -> dict:
    """
    Idem will always succeed in searching for a mod_aggregate function because hub.salt.states is a ReverseSub.
    This function bypasses mod_aggregate by simply returning the input chunk.

    https://gitlab.com/vmware/idem/idem/-/blob/master/idem/idem/mod/aggregate.py
    """
    return chunk
