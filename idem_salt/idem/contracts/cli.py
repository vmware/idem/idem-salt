__contract_order__ = 1000


def pre(hub, ctx):
    """
    After idem loads its config, but before it does anything else,
    load salt's plugins onto the hub with the updated configuration.
    """
    if not hub.OPT.idem.load_salt_modules:
        # Quit before doing anything else
        return

    # Load the minion config from the specified file
    hub.salt.opts = hub.lib.salt.config.minion_config(hub.OPT.idem.minion_config)

    # Overide specific minion config values with the minion_opts from the idem config file
    hub.salt.opts.update(hub.OPT.idem.minion_opts)

    # Hard override some of the minion options to run modules locally
    hub.salt.opts["file_client"] = "local"
    hub.salt.opts["local"] = True
    hub.salt.opts["print_metadata"] = False

    # Generate all the modules that salt will need under hub.salt
    hub.idem.salt.gen_modules(hub.idem.salt.CONTEXT, initial_load=True)

    # Add salt execution modules to hub.exec.salt
    hub.pop.sub.dynamic(
        sub=hub.exec,
        subname="salt",
        resolver=hub.tool.salt.resolve.exec,
        context=None,
    )

    # Allow salt modules to be found without the "salt" prefix
    hub.exec._reverse_sub = hub.exec.salt._reverse_sub

    # Add salt states to hub.states.salt
    hub.pop.sub.dynamic(
        sub=hub.states,
        subname="salt",
        resolver=hub.tool.salt.resolve.state,
        context=None,
    )

    # Allow salt states to be found without the "salt" prefix
    hub.states._reverse_sub = hub.states.salt._reverse_sub

    # Add salt renderers to hub.rend.salt
    hub.pop.sub.dynamic(
        sub=hub.rend,
        subname="salt",
        resolver=hub.tool.salt.resolve.render,
        context=None,
    )
    # Allow salt renderers to be found without the "salt" prefix
    hub.rend._reverse_sub = hub.rend.salt._reverse_sub

    # We already added outputters to the hub earlier, but now we need to do it with the correct OPTS

    # Add space for salt outputters
    hub.pop.sub.add(subname="salt", sub=hub.output)

    # Add all of salt's outputters to the hub
    for outputter in hub.salt.output:
        modname = f"salt.{outputter}"

        # Create a patched mod on the hub for the salt outputter
        mod = hub.lib.pop.loader.LoadedMod(modname)

        # Add the salt outputter function to hub.salt.output as "display"
        mod._attrs["display"] = hub.salt.output[outputter]

        # Ensure that pop-config finds the salt outputters in it's loaded mod check
        hub.output._loaded[f"salt.{outputter}"] = None

        # Load the patched mod onto hub.output.salt
        hub.output.salt._loaded[outputter] = mod

        # Allow salt outputters to be found without the "salt" prefix
        if outputter not in hub.output._loaded:
            hub.output._loaded[outputter] = mod
