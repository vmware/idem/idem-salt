def __init__(hub):
    # Create a sub for python imports
    hub.pop.sub.add(dyne_name="lib")
    hub.pop.sub.add(dyne_name="rend")
    hub.pop.sub.load_subdirs(hub.rend, recurse=True)

    # Import all needed python modules onto the hub
    hub.pop.sub.add(python_import="copy", sub=hub.lib)
    hub.pop.sub.add(python_import="functools", sub=hub.lib)
    hub.pop.sub.add(python_import="importlib", sub=hub.lib)
    hub.pop.sub.add(python_import="inspect", sub=hub.lib)
    hub.pop.sub.add(python_import="io", sub=hub.lib)
    hub.pop.sub.add(python_import="os", sub=hub.lib)
    hub.pop.sub.add(python_import="pkgutil", sub=hub.lib)

    hub.pop.sub.add(python_import="pop", sub=hub.lib)
    hub.lib.importlib.import_module("pop.contract")
    hub.lib.importlib.import_module("pop.loader")

    hub.pop.sub.add(python_import="salt", sub=hub.lib)
    hub.lib.importlib.import_module("salt.config")
    hub.lib.importlib.import_module("salt.loader")
    hub.lib.importlib.import_module("salt.pillar")
    hub.lib.importlib.import_module("salt.syspaths")

    # Create a sub for loaded salt modules
    hub.pop.sub.add(subname="salt")

    # Context for execution modules to use later on
    hub.idem.salt.CONTEXT = {}

    # Add space for salt outputters
    hub.pop.sub.add(subname="salt", sub=hub.output)

    # Get a list of outputters using the default minion opts, we don't have access to the user supplied opts yet
    # And once we do, outputters will have already been processed by pop-config
    tmp_outputters = hub.lib.salt.loader.outputters(
        hub.lib.salt.config.DEFAULT_MINION_OPTS
    )

    # Add space for each of salt's outputters on the hub, they will be loaded for real in gen_modules
    for outputter in tmp_outputters:
        # Ensure that pop-config finds the salt outputters in its loaded_mod check
        hub.output._loaded[f"salt.{outputter}"] = None

        # Allow salt outputters to be found without the "salt" prefix if there isn't a native idem outputter
        if outputter not in hub.output._loaded:
            hub.output._loaded[outputter] = None


def gen_modules(hub, context: dict = None, initial_load: bool = False):
    """
    Tell the salt agent to reload its modules

    CLI Example:

    .. code-block:: bash

        idem exec salt.sys.reload_modules
    """
    if context is None:
        context = {}

    if initial_load:
        hub.salt.grains = hub.salt.opts["grains"] = hub.lib.salt.loader.grains(
            hub.salt.opts
        )
        hub.salt.pillar = hub.salt.opts["pillar"] = hub.lib.salt.pillar.get_pillar(
            hub.salt.opts,
            hub.salt.opts["grains"],
            hub.salt.opts["id"],
            saltenv=hub.salt.opts["saltenv"],
            pillarenv=hub.salt.opts.get("pillarenv"),
        ).compile_pillar()

    hub.salt._imports["utils"] = hub.lib.salt.loader.utils(
        hub.salt.opts, context=context
    )

    hub.salt._imports["functions"] = hub.lib.salt.loader.minion_mods(
        hub.salt.opts, utils=hub.salt.utils, context=context
    )

    hub.salt._imports["serializers"] = hub.lib.salt.loader.serializers(hub.salt.opts)

    hub.salt._imports["states"] = hub.lib.salt.loader.states(
        hub.salt.opts,
        functions=hub.salt.functions,
        utils=hub.salt.utils,
        serializers=hub.salt.serializers,
        context=context,
    )

    hub.salt.functions["sys.reload_modules"] = hub.idem.salt.gen_modules

    hub.salt._imports["rend"] = hub.lib.salt.loader.render(
        hub.salt.opts, functions=hub.salt.functions, context=context
    )

    hub.salt._imports["output"] = hub.lib.salt.loader.outputters(hub.salt.opts)

    # Add all of salt's outputters to the hub
    for outputter in hub.salt.output:
        modname = f"salt.{outputter}"

        # Create a patched mod on the hub for the salt outputter
        mod = hub.lib.pop.loader.LoadedMod(modname)

        # Add the salt outputter function to hub.salt.output as "display"
        mod._attrs["display"] = hub.salt.output[outputter]

        # Ensure that pop-config finds the salt outputters in it's loaded mod check
        hub.output._loaded[modname] = None

        # Load the patched mod onto hub.output.salt
        hub.output.salt._loaded[outputter] = mod

        # Allow salt outputters to be found without the "salt" prefix
        if not hub.output._loaded.get(outputter):
            hub.output._loaded[outputter] = mod
