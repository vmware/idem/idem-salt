import os

import salt.syspaths

CONFIG = {
    "minion_config": {
        "default": os.path.join(salt.syspaths.CONFIG_DIR, "minion"),
        "help": "The location of the minion config file to use with idem",
        "dyne": "idem",
    },
    "minion_opts": {
        "default": {},
        "type": dict,
        "help": "Overrides for the minion config file",
        "dyne": "idem",
    },
    "load_salt_modules": {
        "default": True,
        "type": bool,
        "help": "If set to True then salt modules will be loaded onto idem",
        "dyne": "idem",
    },
}

CLI_CONFIG = {
    "minion_config": {
        "dyne": "idem",
        "subcommands": ["exec", "state"],
    },
}


SUBCOMMANDS = {}

DYNE = {"idem": ["idem"], "tool": ["tool"]}
