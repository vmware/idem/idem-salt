import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add(dyne_name="lib")
    hub.pop.sub.add(python_import="unittest.mock", subname="mock", sub=hub.lib)
    hub.pop.sub.add(python_import="pytest", sub=hub.lib)
    hub.pop.sub.add(python_import="tempfile", sub=hub.lib)

    with hub.lib.mock.patch("sys.argv", ["idem"]):
        hub.pop.config.load(hub.idem.CONFIG_LOAD, cli="idem", parse_cli=False)

    yield hub
