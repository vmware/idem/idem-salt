import tempfile

SALT_STATE = b"""
salt_state:
  salt.test.nop
"""


def test_salt_rend(idem_cli):
    """
    Run a simple salt state with a salt renderer
    """
    with tempfile.NamedTemporaryFile(suffix=".sls", delete=True) as fh:
        fh.write(SALT_STATE)
        fh.flush()
        ret = idem_cli("state", fh.name, "--rend=salt.yamlex")

    assert ret.result is True, ret.stderr
    state_return = next(iter(ret.json.values()))

    assert state_return["result"] is True, state_return["comment"]


def test_implicit_salt_rend(idem_cli):
    """
    Verify that an OG salt renderer is able to run without needing the "salt" prefix
    """
    with tempfile.NamedTemporaryFile(suffix=".sls", delete=True) as fh:
        fh.write(SALT_STATE)
        fh.flush()
        ret = idem_cli("state", fh.name, "--rend=yamlex")

    assert ret.result is True, ret.stderr
    state_return = next(iter(ret.json.values()))

    assert state_return["result"] is True, state_return["comment"]
