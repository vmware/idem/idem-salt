import tempfile

SALT_STATE = b"""
salt_state:
  salt.test.nop
"""


def test_nop(idem_cli):
    """
    Run a simple salt state with idem to verify that it works
    """
    with tempfile.NamedTemporaryFile(suffix=".sls", delete=True) as fh:
        fh.write(SALT_STATE)
        fh.flush()
        ret = idem_cli("state", fh.name)

    assert ret.result is True, ret.stderr
    state_return = next(iter(ret.json.values()))

    assert state_return["result"] is True, state_return["comment"]


LEGACY_SALT_STATE = b"""
salt_state:
  test.nop
"""


def test_legacy_nop(idem_cli):
    """
    Verify that an OG salt state is able to run without needing the "salt" prefix
    """
    with tempfile.NamedTemporaryFile(suffix=".sls", delete=True) as fh:
        fh.write(LEGACY_SALT_STATE)
        fh.flush()
        ret = idem_cli("state", fh.name)

    assert ret.result is True, ret.stderr
    state_return = next(iter(ret.json.values()))

    assert state_return["result"] is True, state_return["comment"]
