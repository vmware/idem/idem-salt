def test_ping(idem_cli):
    """
    Run a salt exec module with idem
    """
    ret = idem_cli("exec", "salt.test.ping")
    assert ret.result is True, ret.stderr
    assert ret.json.result is True, ret.json.comment
    assert ret.json.ret is True
    assert ret.json.ref == "salt.test.ping"


def test_legacy_mod(idem_cli):
    """
    Verify that idem falls back to finding a module under hub.exec.salt if it can't be found under hub.exec
    """
    # Call grains.items from salt explicitly
    ret1 = idem_cli("exec", "salt.grains.items")
    # Call grains.items without specifying the "salt" prefix
    ret2 = idem_cli("exec", "grains.items")

    # Verify that the two commands are identical within reason
    ret1.json.ret.pop("pid")
    ret2.json.ret.pop("pid")
    assert ret1.json == ret2.json
