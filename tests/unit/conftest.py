import pytest


@pytest.fixture(scope="session", name="hub")
def unit_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add(dyne_name="lib")
    hub.pop.sub.add(python_import="unittest.mock", subname="mock", sub=hub.lib)
    hub.pop.sub.add(python_import="pytest", sub=hub.lib)
    yield hub
