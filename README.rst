==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

=========
idem-salt
=========

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/


Idem-Salt extends Idem's functionality by integrating salt capabilities.

About
=====

Idem-Salt gives idem the ability to run salt execution modules directly.
It also exposes salt states to idem and runs them using idem's compiler and engine.

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/vmware/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/vmware/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.8+
* git *(if installing from source, or contributing to the project)*

Installation
------------

.. note::

   If wanting to contribute to the project, and setup your local development
   environment, see the ``CONTRIBUTING.rst`` document in the source repository
   for this project.

If wanting to use ``idem-salt``, you can do so by either
installing from PyPI or from source.

Install from PyPI
+++++++++++++++++

.. code-block:: bash

   pip install idem-salt

Install from source
+++++++++++++++++++

.. code-block:: bash

   # clone repo
   git clone git@gitlab.com/vmware/idem/idem-salt.git
   cd idem-salt

   # Setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -e .

Usage
=====

exec
----

Run a `salt exec module <https://docs.saltproject.io/en/latest/ref/modules/all/index.html>`__ with idem's engine

.. code-block:: bash

   idem exec salt.test.ping

You could even use idem-salt to install packages on any OS:

.. code-block:: bash

   idem exec salt.pkg.install vim

If idem can't find an exec module it will try to fallback to a salt module that matches the ref:

.. code-block:: bash

   idem exec grains.items


states
------

Create an SLS file that contains `salt states <https://docs.saltproject.io/en/latest/ref/states/all/index.html>`__.
To ensure you are getting a salt state and not a native idem state of the same name, prefix the state ref with "salt."

.. code-block:: yaml

   # salt_state.sls

   salt_state:
      salt.test.nop:
         - name: state

Run the salt state with idem's engine:

.. code-block:: bash

   idem state salt_state.sls


The "salt" prefix is only necessary if you want to ensure you are getting a salt state when there's
a native idem state with the same ref.

Idem will fallback to salt states if it can't find a native idem state that matches the given ref.
This ensures backwards compatibility -- your salt states should run with idem out of the box!

.. code-block:: yaml

   salt_state:
      test.nop:
         - name: state
      pkg.installed:
         - name: vim

rend
----

You can also use `salt's renderers <https://docs.saltproject.io/en/latest/ref/renderers/all/index.html>`__ by specifying "salt." before the name of the salt renderer.  I.e.

.. code-block:: bash

   idem state my_state.sls --rend="salt.jinja|salt.yaml"


You can also specify a salt renderer without the "salt." prefix as long as idem doesn't have a native renderer of the same name. I.e.

.. code-block:: bash

   idem state my_state.sls --rend="py|yamlex"


output
------

You can use `salt's outputters <https://docs.saltproject.io/en/latest/ref/output/all/index.html>`__ by specifying "--output=salt.<salt_outputter>"

.. code-block:: bash

   idem state  my_state.sls --output=salt.nested

The "salt." prefix can be omitted if idem doesn't have a native outputter by the same name.

.. code-block:: bash

   idem state  my_state.sls --output=highstate


Notes
=====

Idem offers many new features, such as ESM and argbinding.
Idem also runs states in parallel with asyncio and has a state execution engine that is very different from salt's.
Idem also has it's own event bus.

There are many advantages to be gained from merging these ecosystems.

POP is great because we can extend idem's functionality without needing to touch idem's core -- or the core's of it's POP based dependencies.
Make a plugin to extend idem or any of it's POP-based dependencies. Let's not create a monolith

As a distributed app, you don't need to ask permission from the core maintainers to get your functionality.
Just make your own project that implements the plugin you need!

Roadmap
=======

Reference the `open issues <https://gitlab.com/vmware/idem/idem-salt/issues>`__ for a list of
proposed features (and known issues).

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.
